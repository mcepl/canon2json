#!/usr/bin/env python
# https://github.com/zefanja/swordjs/tree/master/data
from __future__ import print_function

import collections
import json
import re
import sys

from ast import literal_eval as make_tuple


def parse_books(instr):
    tup = make_tuple('(%s)' % instr.replace('{', '(').replace('}', ')'))[:-1]
    out = []
    for bk in tup:
        out.append({'name': bk[0], 'abbrev': bk[1], 'maxChapter': bk[3]})
    # out: {"name": "Genesis", "abbrev": "Gen", "maxChapter": 50},
    # in: ('Genesis', 'Gen', 'Gen', 50),
    return tuple(out)

canon_text = sys.stdin.read()

otbooks_raw = re.search(r'struct sbook otbooks_\S+ = {(.+?)};',
                        canon_text, re.DOTALL)

ntbooks_raw = re.search(r'struct sbook ntbooks_\S+ = {(.+?)};',
                        canon_text, re.DOTALL)

vm_raw = re.search(r'int vm_(\S+) = {(.+?)};',
                   canon_text, re.DOTALL)

assert otbooks_raw is not None \
    and ntbooks_raw is not None and vm_raw is not None, \
    '''Some part of the canon file is missing!

    Check the header file whether it has all three structs defining
    otbooks, ntbooks, and vm. If any one is missing, copy it from the
    appropriate other file (follow the comments in the canon file)
    '''

otbooks = parse_books(otbooks_raw.group(1))
ntbooks = parse_books(ntbooks_raw.group(1))

verses = []
b_verses = []
vm_str = vm_raw.group(2).strip()
for line in vm_str.split('\n'):
    line = line.strip()
    if not line:
        continue
    elif line.startswith('//'):
        if b_verses:
            verses.append(b_verses)
        b_verses = []
    else:
        b_verses.extend(make_tuple('(%s)' % line))

verses.append(b_verses)

out = collections.OrderedDict()
out['ot'] = otbooks
out['nt'] = ntbooks
out['versesInChapter'] = verses

print(json.dumps(out, indent=4))
